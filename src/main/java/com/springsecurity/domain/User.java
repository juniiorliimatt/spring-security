package com.springsecurity.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.SEQUENCE;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 22/11/2021
 */

@Entity
@Table(name = "users")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User{

  @Id
  @SequenceGenerator(name = "user_sequence", sequenceName = "user_sequence", allocationSize = 1)
  @GeneratedValue(strategy = SEQUENCE, generator = "user_sequence")
  @Column(name = "id", updatable = false)
  private Long id;

  private String name;
  private String username;
  private String password;

  @ManyToMany(fetch = EAGER)
  private Collection<Role> roles = new ArrayList<>();
}
