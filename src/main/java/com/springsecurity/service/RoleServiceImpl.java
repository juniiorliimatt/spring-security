package com.springsecurity.service;

import com.springsecurity.domain.Role;
import com.springsecurity.repository.RoleRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 22/11/2021
 */

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class RoleServiceImpl implements RoleService{
  private final RoleRepository roleRepository;

  @Override
  public Role saveRole(Role role){
    log.info("Saving new role {} to the database", role.getName());
    return roleRepository.save(role);
  }
}
