package com.springsecurity.service;

import com.springsecurity.domain.Role;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 22/11/2021
 */

public interface RoleService{
  Role saveRole(Role role);
}
