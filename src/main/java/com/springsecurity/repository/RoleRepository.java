package com.springsecurity.repository;

import com.springsecurity.domain.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 22/11/2021
 */

@Repository
public interface RoleRepository extends JpaRepository<Role, Long>{
  Role findByName(String name);
}
