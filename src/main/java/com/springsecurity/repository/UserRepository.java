package com.springsecurity.repository;

import com.springsecurity.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 22/11/2021
 */

@Repository
public interface UserRepository extends JpaRepository<User, Long>{
  User findByUsername(String username);
}
