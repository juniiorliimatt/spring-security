package com.springsecurity.controller;

import lombok.Data;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 22/11/2021
 */

@Data
public class RoleToUserForm{
  private String username;
  private String roleName;
}
