package com.springsecurity.controller;

import com.springsecurity.domain.Role;
import com.springsecurity.service.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 22/11/2021
 */

@RestController
@RequestMapping("/api/role")
@RequiredArgsConstructor
public class RoleController{
  private final RoleService roleService;

  @PostMapping("/save")
  public ResponseEntity<Role> saveUser(@RequestBody Role role){
    URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/role/save").toUriString());
    return ResponseEntity.created(uri).body(roleService.saveRole(role));
  }
}
