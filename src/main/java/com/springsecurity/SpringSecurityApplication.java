package com.springsecurity;

import com.springsecurity.domain.Role;
import com.springsecurity.domain.User;
import com.springsecurity.service.RoleService;
import com.springsecurity.service.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 22/11/2021
 */

@SpringBootApplication
public class SpringSecurityApplication{

  public static void main(String[] args){
    SpringApplication.run(SpringSecurityApplication.class, args);
  }

  @Bean
  PasswordEncoder passwordEncoder(){
    return new BCryptPasswordEncoder();
  }

  @Bean
  CommandLineRunner rum(UserService userService, RoleService roleService){
    return args -> {
      roleService.saveRole(new Role(null, "ROLE_USER"));
      roleService.saveRole(new Role(null, "ROLE_MANAGER"));
      roleService.saveRole(new Role(null, "ROLE_ADMIN"));
      roleService.saveRole(new Role(null, "ROLE_SUPER_ADMIN"));

      userService.saveUser(new User(null, "John Travolta", "john", "1234", new ArrayList<>()));
      userService.saveUser(new User(null, "Will Smith", "will", "1234", new ArrayList<>()));
      userService.saveUser(new User(null, "Jim Carry", "jim", "1234", new ArrayList<>()));
      userService.saveUser(new User(null, "Arnold Schwarzenegger", "arnold", "1234", new ArrayList<>()));

      userService.addRoleToUser("john", "ROLE_USER");
      userService.addRoleToUser("will", "ROLE_MANAGER");
      userService.addRoleToUser("jim", "ROLE_ADMIN");

      userService.addRoleToUser("arnold", "ROLE_USER");
      userService.addRoleToUser("arnold", "ROLE_MANAGER");
      userService.addRoleToUser("arnold", "ROLE_SUPER_ADMIN");
    };
  }
}
